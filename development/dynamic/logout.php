<?php
	session_start();
	require('dbconnection.php');
	// Destroying All Sessions
	if(session_destroy())
	{
		header("Location: login.php");
	}
?>