<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Equipment Inventory</title>
    <link rel="stylesheet" href="../assets/css/template.css"/>
    <link rel="stylesheet" href="../assets/uikit/css/uikit.almost-flat.min.css"/>
    <link rel="stylesheet" href="../assets/uikit/css/components/accordion.almost-flat.min.css"/>
    <link rel="stylesheet" href="../assets/mdl/material.min.css" />
    <link rel="stylesheet" href="../assets/fonts/iconfont/material-icons.css"/>


</head>
<body onload="mytime()">
    <div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
        <header class="mdl-layout__header">
            <div class="mdl-layout__header-row">
                <img class="mdl-cell--hide-phone" src="../assets/img/cvsu_logo.png" alt="" width="50" height="50"/>
                <span class="mdl-layout-title uk-margin-left mdl-cell--hide-phone">Equipment Inventory</span>
                <div class="mdl-layout-spacer"></div>
                <nav class="mdl-navigation">
                    <a class="mdl-navigation__link" href=""><i class="material-icons mdl-badge mdl-badge--overlap" data-badge="69">notifications</i></a>
                    <a class="mdl-navigation__link" href="../index.html">Logout</a>
                </nav>
            </div>
        </header>
        <div class="mdl-layout__drawer" id="scroll">
            <div class="sidemenu-drawer-header sidemenu-image">
                <img class="mdl-layout--small-screen-only uk-margin-bottom" src="../assets/img/cvsu_logo.png" alt="" width="50" height="50"/>
                <h4 id="timehere" class="mdl-color-text--white mdl-layout--large-screen-only"></h4>
                <span class="mdl-layout-title mdl-color-text--white">Menu</span>
            </div>
            <nav class="mdl-navigation">
                <a class="mdl-navigation__link active-link" href="#"><i class="material-icons">home</i> Home</a>
                <a class="mdl-navigation__link accordion" onclick="return myFunction('panel-1');"><i class="material-icons">dashboard</i>Inventory<i class="mdl-layout-spacer"></i></a>
                <div id="panel-1" class="panel">
                    <a class="mdl-navigation__link" href="inventory/property-custodian.html">Property Custodian</a>
                    <a class="mdl-navigation__link" href="inventory/ccl.html">CCL</a>
                </div>
                <a class="mdl-navigation__link accordion" onclick="return myFunction('panel-2');"><i class="material-icons">swap_horiz</i>Transaction<i class="mdl-layout-spacer"></i></a>
                <div id="panel-2" class="panel">
                    <a class="mdl-navigation__link" href="transaction/borrow.html">Borrow Equipment</a>
                    <a class="mdl-navigation__link" href="transaction/return.html">Return Equipment</a>
                </div>
                <a class="mdl-navigation__link accordion" onclick="return myFunction('panel-3');"><i class="material-icons">view_list</i>List<i class="mdl-layout-spacer"></i></a>
                <div id="panel-3" class="panel">
                    <a class="mdl-navigation__link" href="list/property-custodian.html">Property Custodian</a>
                    <a class="mdl-navigation__link" href="list/ccl.html">CCL</a>
                </div>
                <a class="mdl-navigation__link" href="account/account-list.html"><i class="material-icons">account_box</i> Account</a>
                <div class="uk-margin-large-bottom"></div>
            </nav>
        </div>
        <main class="mdl-layout__content">
            <div class="mdl-padding">
                <div class="mdl-grid">
                    <div class="mdl-color--white mdl-shadow--2dp mdl-cell mdl-cell--12-col mdl-grid">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque consequatur deleniti deserunt dignissimos dolorum earum est eum hic, molestiae neque omnis pariatur qui quia soluta veniam veritatis vitae voluptate! Consectetur!
                    </div>

                    <div class=" mdl-shadow--2dp mdl-color--white mdl-cell mdl-cell--8-col">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet architecto beatae consequuntur laudantium maiores nemo quam similique. A asperiores blanditiis eum facere labore nisi odit repudiandae vel? Atque, ullam.
                    </div>

                    <div class=" mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-grid mdl-grid--no-spacing">
                        <div class=" mdl-card mdl-shadow--2dp mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet mdl-cell--12-col-desktop">
                            <div class="mdl-card__title mdl-card--expand mdl-color--teal-300">
                                <h2 class="mdl-card__title-text">Updates</h2>
                            </div>
                            <div class="mdl-card__supporting-text mdl-color-text--grey-600">
                                Non dolore elit adipisicing ea reprehenderit consectetur culpa.
                            </div>
                            <div class="mdl-card__actions mdl-card--border">
                                <a href="#" class="mdl-button mdl-js-button mdl-js-ripple-effect">Read More</a>
                            </div>
                        </div>
                        <div class="mdl-cell--1-col"></div>
                        <div class="mdl-card mdl-color--deep-purple-500 mdl-shadow--2dp mdl-cell mdl-cell--4-col mdl-cell--3-col-tablet mdl-cell--12-col-desktop">
                            <div class="mdl-card__supporting-text mdl-color-text--blue-grey-50">
                                <h3>View options</h3>
                                <ul>
                                    <li>
                                        <label for="chkbox1" class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect">
                                            <input type="checkbox" id="chkbox1" class="mdl-checkbox__input">
                                            <span class="mdl-checkbox__label">Click per object</span>
                                        </label>
                                    </li>
                                    <li>
                                        <label for="chkbox2" class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect">
                                            <input type="checkbox" id="chkbox2" class="mdl-checkbox__input">
                                            <span class="mdl-checkbox__label">Views per object</span>
                                        </label>
                                    </li>
                                    <li>
                                        <label for="chkbox3" class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect">
                                            <input type="checkbox" id="chkbox3" class="mdl-checkbox__input">
                                            <span class="mdl-checkbox__label">Objects selected</span>
                                        </label>
                                    </li>
                                    <li>
                                        <label for="chkbox4" class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect">
                                            <input type="checkbox" id="chkbox4" class="mdl-checkbox__input">
                                            <span class="mdl-checkbox__label">Objects viewed</span>
                                        </label>
                                    </li>
                                </ul>
                            </div>
                            <div class="mdl-card__actions mdl-card--border">
                                <a href="#" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-color-text--blue-grey-50">Change location</a>
                                <div class="mdl-layout-spacer"></div>
                                <i class="material-icons">location_on</i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</body>
    <script src="../assets/uikit/js/components/accordion.min.js"></script>
    <script src="../assets/uikit/js/uikit.min.js"></script>
    <script src="../assets/mdl/material.min.js"></script>
    <script src="../assets/js/clock.js"></script>
    <script src="../assets/js/scrollbar.js"></script>
    <script src="../assets/js/accordion.js"></script>
</html>