/**
 * Created by tams on 01/19/18.
 */

function myFunction(id) {
    var x = document.getElementById(id);
    if (x.className.indexOf("panel-show") == -1) {
        x.className += " panel-show";
    } else {
        x.className = x.className.replace(" panel-show", "");
    }
}