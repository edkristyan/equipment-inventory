/**
 * Created by tams on 01/26/18.
 */
function mytime()
{
    var d=new Date();
    ap="AM";
    h=d.getHours();
    m=d.getMinutes();
    s=d.getSeconds();
    if (h>11) { ap = "PM"; }
    if (h>12) { h = h-12; }
    if (h==0) { h = 12; }
    if (m<10) { m = "0" + m; }
    if (s<10) { s = "0" + s; }
    document.getElementById('timehere').innerHTML=h + ":" + m + "" + " " + ap;
    t=setTimeout('mytime()',500);
}