<?php 

    require('dbconnection.php');
    session_start();

    $error=''; 
    if(isset($_POST['btn-login'])){
        if (empty($_POST['txt_username']) || empty($_POST['txt_password'])) {
            $error = "Username or Password is invalid";
        }
        else
        {

            $username = $_POST['txt_username'];
            $password = $_POST['txt_password'];


            $query = mysqli_query($con, "SELECT * from tblaccounts where username = '$username' and password = '$password'");
            $row = mysqli_num_rows($query);

            if($row > 0)
            {
                while($row = mysqli_fetch_array($query)){
                  $_SESSION['role'] = $row['type_id'];
                  $_SESSION['userid'] = $row['user_id'];
                }    
                header ('location: equipment-inventory/home.php');
            }
             else
                {
                   $error='Invalid Username or Password!';
                }
        }

    }


 ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Equipment Inventory</title>
    <link rel="stylesheet" href="assets/css/template.css"/>
    <link rel="stylesheet" href="assets/uikit/css/uikit.almost-flat.min.css"/>
    <link rel="stylesheet" href="assets/mdl/material.min.css"/>
    <link rel="stylesheet" href="assets/fonts/iconfont/material-icons.css" />

</head>
<body>
    <div class="hero">
        <div class="mdl-color-text--white mdl-layout mdl-js-layout mdl-layout--fixed-drawer login-container">
            <div class="mdl-layout__drawer drawer-design">
                <div class="mdl-layout-spacer"></div>
                <form action="" method="post">
                    <h3 class="mdl-color-text--deep-orange-500">Login</h3>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input class="mdl-textfield__input mdl-color-text--white" type="text" id="user" name="txt_username">
                        <label class="mdl-textfield__label" for="user">Username</label>
                    </div>
                    <br>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input class="mdl-textfield__input mdl-color-text--white" type="password" id="pass" name="txt_password">
                        <label class="mdl-textfield__label" for="pass">Password</label>
                    </div>
                    <div class="uk-margin-top">
                        <button type="submit" name="btn-login" class="mdl-color--deep-orange-500 mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn-width">Sign in</button>
                        <span class=" mdl-color-text--red content-center"><?php echo $error; ?></span>
                    </div>
                </form>
                <div class="mdl-layout-spacer"></div>
            </div>

            <main class="mdl-layout__content">
                <div class="uk-container uk-container-center uk-margin-large-top">
                    <div class="uk-text-center uk-margin-large-top">
                        <img src="assets/img/cvsu_logo.png" alt="CVSU logo" width="250" height="250"/>
                        <h4 class="mdl-color-text--white">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</h4>
                    </div>
                </div>
            </main>
        </div>
    </div>
</body>

<script src="assets/uikit/js/uikit.min.js"></script>
<script src="assets/mdl/material.min.js"></script>
</html>